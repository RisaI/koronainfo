using System;
using System.Collections.Generic;
using System.Linq;

namespace koronainfo.Data
{
    public class SIRPDaily
    {
        public DateTime Date { get; private set; }

        public float DataI { get; private set; }
        public float DataD { get; private set; }
        public float DataR { get; private set; }
        public float DataIMin { get; private set; }
        public float DataDMin { get; private set; }
        public float DataRMin { get; private set; }
        public float DataIMax { get; private set; }
        public float DataDMax { get; private set; }
        public float DataRMax { get; private set; }

        public static SIRPDaily Parse(DateTime date, string line)
        {
            var s = line.Split(';');

            return new SIRPDaily {
                Date = date,
                DataI = float.Parse(s[0]),
                DataIMin = float.Parse(s[1]),
                DataIMax = float.Parse(s[2]),
                DataD = float.Parse(s[3]),
                DataDMin = float.Parse(s[4]),
                DataDMax = float.Parse(s[5]),
                DataR = float.Parse(s[6]),
                DataRMin = float.Parse(s[7]),
                DataRMax = float.Parse(s[8])
            };
        }
    }
}