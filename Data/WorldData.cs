using System;
using System.Collections.Generic;
using System.Linq;

namespace koronainfo.Data
{
    public class WorldData : IBalance
    {
        public ISource Source { get; set; }

        public int? Tested { get; } = null;
        public int Cases { get; private set; }
        public int Deaths { get; private set; }
        public int Recoveries { get; private set; }

        public IEnumerable<DailyBalance> Daily { get; private set; }

        public WorldData(ISource source, IEnumerable<DailyBalance> daily)
        {
            Source = source;
            Daily = daily;

            Tested = daily.First().Tested;
            Cases = daily.First().Cases;
            Deaths = daily.First().Deaths;
            Recoveries = daily.First().Recoveries;
        }
    }
}