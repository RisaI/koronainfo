namespace koronainfo.Data
{
    public interface IBalance
    {
        int? Tested { get; }
        int Cases { get; }
        int Deaths { get; }
        int Recoveries { get; }
    }
}