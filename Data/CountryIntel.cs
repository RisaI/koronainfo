using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace koronainfo.Data
{
    public class CountryIntel
    {
        private string _DisplayName;
        [JsonPropertyName("displayName")]
        public string DisplayName
        {
            get { return _DisplayName ?? ID; }
            set { _DisplayName = value; }
        }
        [JsonPropertyName("id")]
        public string ID { get; set; }
        [JsonPropertyName("wldmetersName")]
        public string _WorldometersName { get; set; }
        [JsonIgnore]
        public string WorldometersName { get { return _WorldometersName ?? ID; } }

        [JsonPropertyName("x")]
        public float X { get; set; }
        [JsonPropertyName("y")]
        public float Y { get; set; }

        [JsonPropertyName("population")]
        public int? Population { get; set; }

        [JsonPropertyName("regions")]
        public CountryRegion[] Regions { get; set; }
    }

    public class CountryRegion
    {
        private string _DisplayName;
        [JsonPropertyName("displayName")]
        public string DisplayName
        {
            get { return _DisplayName ?? ID; }
            set { _DisplayName = value; }
        }
        [JsonPropertyName("id")]
        public string ID { get; set; }
        [JsonPropertyName("polygon")]
        public Polygon Polygon { get; set; }
    }

    public class Polygon
    {
        [JsonPropertyName("outer")]
        public float[][] Outer { get; set; }
        [JsonPropertyName("holes")]
        public float[][][] Holes { get; set; }

        public void Scale(float x, float y)
        {
            foreach (var polygon in ToLeafletFormat())
            {
                foreach (var point in polygon)
                {
                    point[0] *= y;
                    point[1] *= x;
                }
            }
        }

        public void Translate(float x, float y)
        {
            foreach (var polygon in ToLeafletFormat())
            {
                foreach (var point in polygon)
                {
                    point[0] += y;
                    point[1] += x;
                }
            }
        }

        public IEnumerable<float[][]> ToLeafletFormat()
        {
            if (Holes != null)
                return Holes.Prepend(Outer);

            return new [] { Outer };
        }
    }
}