using System;

namespace koronainfo.Data
{
    public class DailyBalance : IBalance
    {
        public DateTime Date { get; set; }
        public int? Tested { get; set; }
        public int Cases { get; set; }
        public int Deaths { get; set; }
        public int Recoveries { get; set; }
        
        public int? DeltaTested { get; set; }
        public int DeltaCases { get; set; }
        public int DeltaDeaths { get; set; }
        public int DeltaRecoveries { get; set; }

        public void SetDeltaFrom(DailyBalance day)
        {
            DeltaTested = Tested - day.Tested;
            DeltaCases = Cases - day.Cases;
            DeltaDeaths = Deaths - day.Deaths;
            DeltaRecoveries = Recoveries - day.Recoveries;
        }
    }
}