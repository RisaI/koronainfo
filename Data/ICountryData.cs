using System;
using System.Collections.Generic;

namespace koronainfo.Data
{
    public interface ICountryData : IBalance
    {
        ISource Source { get; }
        CountryIntel Intel { get; set; }

        IEnumerable<DailyBalance> Daily { get; }
        IEnumerable<RegionalBalance> Regional { get; }
    }

    public class RegionalBalance : IBalance
    {
        public string ID { get; set; }

        public int? Tested { get; set; }
        public int Cases { get; set; }
        public int Deaths { get; set; }
        public int Recoveries { get; set; }
    }
}