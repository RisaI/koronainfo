using System;
using System.Collections.Generic;
using System.Linq;

namespace koronainfo.Data
{
    public class STIRDDaily
    {
        public DateTime Date { get; set; }
        
        public float DataT { get; set; }
        public float DataI { get; set; }
        public float DataR { get; set; }
        public float DataD { get; set; }
        public float SigmaT { get; set; }
        public float SigmaI { get; set; }
        public float SigmaR { get; set; }
        public float SigmaD { get; set; }

        public static STIRDDaily Parse(string line)
        {
            var s = line.Split(';');

            return new STIRDDaily {
                Date = DateTime.ParseExact(s[0], "dd-MM-yyyy", null),
                
                DataT = float.Parse(s[1]),
                DataI = float.Parse(s[2]),
                DataR = float.Parse(s[3]),
                DataD = float.Parse(s[4]),

                SigmaT = float.Parse(s[5]),
                SigmaI = float.Parse(s[6]),
                SigmaR = float.Parse(s[7]),
                SigmaD = float.Parse(s[8]),
            };
        }
    }
}