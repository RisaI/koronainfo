using System;
using System.Collections.Generic;
using System.Linq;

using koronainfo.Components;

namespace koronainfo
{
    public static class PlotUtils
    {
        public static IEnumerable<PlotData> DataWithConfidence(IEnumerable<(object, float, float, float)> data, string text, string legendGroup, string lineColor, (object, float)? prependCertain = null)
        {
            var xaxis = data.Select(d => d.Item1).PrependIfNotNull(prependCertain.Value.Item1);
            return new [] {
                new PlotData() {
                    Name = "Horní odhad",
                    Line = new PlotLine { Color = "#00000000", Shape = PlotLine.ShapeMode.Spline },
                    Mode = PlotData.DataMode.Lines,
                    ShowLegend = false,
                    LegendGroup = legendGroup,
                    HoverInfo = "skip",
                    X = xaxis,
                    Y = data.Select(d => (object)d.Item4).PrependIfNotNull(prependCertain?.Item2)
                },
                new PlotData() {
                    Name = "Dolní odhad",
                    Line = new PlotLine { Color = "#00000000", Shape = PlotLine.ShapeMode.Spline },
                    Mode = PlotData.DataMode.Lines,
                    Fill = PlotData.FillMode.ToNextY,
                    FillColor = $"{lineColor}50",
                    ShowLegend = false,
                    LegendGroup = legendGroup,
                    HoverInfo = "skip",
                    X = xaxis,
                    Y = data.Select(d => (object)d.Item3).PrependIfNotNull(prependCertain?.Item2)
                },
                new PlotData() {
                    Name = text,
                    Line = new PlotLine { Color = lineColor, Dash = PlotLine.DashMode.Dot, Shape = PlotLine.ShapeMode.Spline },
                    Mode = PlotData.DataMode.LinesMarkers,
                    LegendGroup = legendGroup,
                    HoverInfo = "x+text",
                    X = xaxis,
                    Y = data.Select(d => (object)d.Item2).PrependIfNotNull(prependCertain?.Item2),
                    Texts = data.Select(d => $"{text}: {d.Item2.Pretty()} (max: {d.Item4.Pretty()}, min: {d.Item3.Pretty()})").PrependIfNotNull(prependCertain != null ? string.Empty : null)
                }
            };
        }

        public static IEnumerable<PlotData> DisconnectFirst<T1,T2>(IEnumerable<(T1, T2)> data, string text, string appendInfo, string legendGroup, string lineColor, int disconnectedPoints = 1) where T1 : class where T2 : class
        {
            return DisconnectFirst(data.Select(d => d.Item1), data.Select(d => d.Item2), text, appendInfo, legendGroup, lineColor, disconnectedPoints);
        }

        public static IEnumerable<PlotData> DisconnectFirst<T1,T2>(
            IEnumerable<T1> xData,
            IEnumerable<T2> yData,
            string text,
            string appendInfo,
            string legendGroup,
            string lineColor,
            int disconnectedPoints = 1,
            PlotLine.DashMode dash = PlotLine.DashMode.Solid

            ) where T1 : class where T2 : class
        {
            return new [] {
                    new PlotData() {
                        Name = text,
                        Line = new PlotLine { Color = lineColor, Dash = dash },
                        LegendGroup = legendGroup,
                        X = xData.Skip(disconnectedPoints),
                        Y = yData.Skip(disconnectedPoints)
                    },
                    new PlotData() {
                        Name = $"{text} ({appendInfo})",
                        Line = new PlotLine { Color = lineColor },
                        Marker = new PlotMarker { Size = 9, Color = "#00000000", Line = new PlotLine { Color = lineColor } },
                        Mode = PlotData.DataMode.Markers,
                        LegendGroup = legendGroup,
                        ShowLegend = false,
                        X = xData.Take(disconnectedPoints),
                        Y = yData.Take(disconnectedPoints)
                    }
            };
        }

        static IEnumerable<T> PrependIfNotNull<T>(this IEnumerable<T> e, T obj) where T : class
        {
            if (obj != null)
                return e.Prepend(obj);

            return e;
        }
    }
}