using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace koronainfo.Services.Scraper
{
    public abstract class DataSource<T> : IDataSource where T : class
    {
        public string SourceName { get; private set; }
        public string SourceURL { get; private set; }

        public bool ProvidesCountryData { get; protected set; }
        public bool ProvidesWorldData { get; protected set; }

        public IEnumerable<Data.ICountryData> CountryData { get; private set; }
        public Data.WorldData WorldData { get; private set; }

        public DateTime LastFetchTime { get; private set; }

        protected ILogger Logger { get; private set; }
        protected IConfiguration Config { get; private set; }
        protected DataContainer Container { get; private set; }

        private FileSystemWatcher Watcher;

        public string StoragePath { get { return System.IO.Path.Combine(Config["Sources:StorageRoot"], SourceName); } }

        public DataSource(Services.DataContainer container, string name, string url)
        {
            SourceName = name;
            SourceURL = url;

            Logger = container.LoggerFactory.CreateLogger(GetType());
            Config = container.Config;
            Container = container;

            if (!Directory.Exists(StoragePath))
                Directory.CreateDirectory(StoragePath);

            // File system watching
            Watcher = new FileSystemWatcher(StoragePath);
            Watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName;
            Watcher.Filter = "*.json";

            Watcher.Created += OnFileEvent;
            Watcher.Changed += OnFileEvent;
            
            // Set initial data
            var latest = GetLatest();
            if (latest.HasValue)
                try
                {
                    SetFetched(JsonSerializer.Deserialize<T>(File.ReadAllText(GetFileName(latest.Value))), latest.Value, false);
                }
                catch
                {
                    Fetch(true).GetAwaiter().GetResult();
                }
            else
                Fetch(true).GetAwaiter().GetResult();

            Watcher.EnableRaisingEvents = true;
        }

        protected abstract Task<(T, DateTime)> FetchData();
        protected abstract IEnumerable<Data.ICountryData> CalculateCountryData(T data);
        protected abstract Data.WorldData CalculateWorldData(T data);

        protected T LastFetch { get; private set; }
        public async Task Fetch(bool throwOnFailure)
        {
            try
            {
                Logger.LogDebug($"Fetching new data.");
                var (fetched, time) = await FetchData();

                if (LastFetchTime >= time)
                    return;

                var latest = GetLatest();
                if (latest.HasValue)
                {
                    // Check for diffs
                    var prevContent = await File.ReadAllTextAsync(GetFileName(latest.Value));
                    var nextContent = JsonSerializer.Serialize(fetched);

                    if (prevContent == nextContent)
                        return;
                }

                Logger.LogInformation($"Serializing new data to '{time.Ticks}.json'");

                Watcher.EnableRaisingEvents = false;
                using (var stream = new FileStream(GetFileName(time), FileMode.Create, FileAccess.Write))
                {
                    await JsonSerializer.SerializeAsync(stream, fetched);
                }
                Watcher.EnableRaisingEvents = true;

                SetFetched(fetched, time, false);

            }
            catch (Exception ex)
            {
                Logger.LogCritical(ex, "An error has occured while fetching data.");

                if (throwOnFailure)
                    throw ex;

                return;
            }
        }

        private string _prevFile;
        private DateTime _prevFileTime;
        private void OnFileEvent(object sender, FileSystemEventArgs e)
        {
            var name = Path.GetFileNameWithoutExtension(e.FullPath);
            DateTime time;

            if ((_prevFile != e.Name || _prevFileTime.AddSeconds(1) < DateTime.Now) &&
                name.All(c => char.IsDigit(c)) && (time = new DateTime(long.Parse(name))) >= LastFetchTime)
            {
                _prevFile = e.Name;
                _prevFileTime = DateTime.Now;
                Logger.LogInformation("Hotswapping data from filesystem.");
                try
                {
                    SetFetched(ReadFromFile(time).GetAwaiter().GetResult(), time, true);
                }
                catch (Exception ex)
                {
                    Logger.LogCritical(ex, "Failed to load JSON from filesystem.");
                }
            }
        }

        public event Action<IDataSource> InternalChange;

        private void SetFetched(T fetch, DateTime timestamp, bool intern)
        {
            LastFetch = fetch;
            LastFetchTime = timestamp;

            CountryData = CalculateCountryData(fetch);
            WorldData = CalculateWorldData(fetch);

            if (intern)
                InternalChange?.Invoke(this);
        }

        protected IEnumerable<DateTime> GetAvailableBackups()
        {
            return Directory.GetFiles(StoragePath).Select(f => Path.GetFileNameWithoutExtension(f)).Where(f => f.All(c => char.IsDigit(c))).Select(f => new DateTime(long.Parse(f)));
        }

        protected DateTime? GetLatest()
        {
            var available = GetAvailableBackups();
            return available.Count() <= 0 ? null : (DateTime?)available.Max();
        }

        public string GetFileName(DateTime timestamp) => Path.Combine(StoragePath, $"{timestamp.Ticks}.json");

        protected async Task<T> ReadFromFile(DateTime timestamp)
        {
            using (var stream = new FileStream(GetFileName(timestamp), FileMode.Open, FileAccess.Read))
                return await JsonSerializer.DeserializeAsync<T>(stream);
        }

        public virtual void Dispose()
        {
            Watcher.Dispose();
        }
    }

    public interface IDataSource : ISource, IDisposable
    {
        bool ProvidesCountryData { get; }
        bool ProvidesWorldData { get; }

        IEnumerable<Data.ICountryData> CountryData { get; }
        Data.WorldData WorldData { get; }

        DateTime LastFetchTime { get; }

        Task Fetch(bool throwOnFailure);
        event Action<IDataSource> InternalChange;
    }
}