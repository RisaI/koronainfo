using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using koronainfo.Data;

namespace koronainfo.Services.Scraper.NCZI
{
    public class Scraper : DataSource<CompleteReport>
    {
        public const string Name = "NCZI", URL = "https://covid-19.nczisk.sk/sk";
        const string ApiUrl = "https://covid-19.nczisk.sk/webapi/v1/kpi", PositivesPath = "/5/data", NegativesPath = "/23/data", RecoveriesPath = "/7/data", DeathsPath = "/8/data";

        public Scraper(DataContainer container)
            : base(container, Name, URL)
        {
            ProvidesCountryData = true;
        }

        protected override IEnumerable<ICountryData> CalculateCountryData(CompleteReport data)
        {
            var recoveries = data.DailyRecoveries ?? Enumerable.Empty<DailyData>();
            var deaths = data.DailyDeaths ?? Enumerable.Empty<DailyData>();

            // Calculate daily data
            var daily = new List<Data.DailyBalance>() {
                new Data.DailyBalance() {
                    Date = DateTime.Today,
                    Tested = data.DailyPositive.Max(p => p.Count) + data.DailyNegative.Max(p => p.Count),
                    Cases = data.DailyPositive.Max(p => p.Count),
                    Recoveries = recoveries.Count() > 0 ? recoveries.Max(d => d.Count) : 0,
                    Deaths = deaths.Count() > 0 ? deaths.Max(d => d.Count) : 0
                }
            };

            foreach (var day in data.DailyPositive.Where(d => d.ParsedDate < DateTime.Today))
            {
                var topmostNegative = data.DailyNegative.Where(d => d.ParsedDate <= day.ParsedDate).OrderByDescending(d => d.ParsedDate).FirstOrDefault();
                var topmostRecoveries = recoveries.Where(d => d.ParsedDate <= day.ParsedDate).OrderByDescending(d => d.ParsedDate).FirstOrDefault();
                var topmostDeaths = deaths.Where(d => d.ParsedDate <= day.ParsedDate).OrderByDescending(d => d.ParsedDate).FirstOrDefault();

                daily.Add(
                    new Data.DailyBalance {
                        Date = day.ParsedDate,
                        Tested = (day.Count + topmostNegative?.Count ?? 0),
                        Cases = day.Count,
                        Recoveries = topmostRecoveries?.Count ?? 0,
                        Deaths = topmostDeaths?.Count ?? 0,
                    }
                );
            }

            if (!daily.Any(d => d.Date == DateTime.Today.AddDays(-1)))
            {
                daily.Insert(1, new Data.DailyBalance {
                    Date = DateTime.Today.AddDays(-1),
                    Tested = data.DailyPositive.Max(p => p.Count) + data.DailyNegative.Max(p => p.Count),
                    Cases = data.DailyPositive.Max(p => p.Count),
                    Recoveries = recoveries.Count() > 0 ? recoveries.Max(d => d.Count) : 0,
                    Deaths = deaths.Count() > 0 ? deaths.Max(d => d.Count) : 0
                });
            }

            // Order and subtract to get deltas
            daily = daily.OrderByDescending(d => d.Date).ToList();
            for (int i = 0; i < daily.Count - 1; ++i)
                daily[i].SetDeltaFrom(daily[i + 1]);

            var skIntel = Container.CountryIntel.First(i => i.ID == "Slovakia");

            return new [] { new NCZI.CountryData(daily, skIntel) { Source = this } };
        }

        protected override WorldData CalculateWorldData(CompleteReport data)
        {
            return null;
        }

        static readonly Regex TokenRegex = new Regex(@"XSRF-TOKEN=([^;]+);");
        protected override async Task<(CompleteReport, DateTime)> FetchData()
        {
            var report = new CompleteReport();

            using (var client = new HttpClient())
            {
                var token = string.Empty;

                using (var resp = await client.GetAsync(URL))
                {
                    var cookies = resp.Headers.GetValues("Set-Cookie");

                    token = System.Web.HttpUtility.UrlDecode(TokenRegex.Match(cookies.First(c => TokenRegex.IsMatch(c))).Groups[1].Value);
                }

                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("X-XSRF-TOKEN", token);

                report.DailyPositive = await PostData(client, ApiUrl + PositivesPath);
                report.DailyNegative = await PostData(client, ApiUrl + NegativesPath);
                report.DailyRecoveries = await PostData(client, ApiUrl + RecoveriesPath);
                report.DailyDeaths = await PostData(client, ApiUrl + DeathsPath);
            }

            return (report, DateTime.Now);
        }

        protected async Task<DailyData[]> PostData(HttpClient client, string path)
        {
            using (var content = new StringContent($"{{\"from\":\"2020-03-06\",\"to\":\"{DateTime.Today.ToString("yyyy-MM-dd")}\",\"period\":\"d\"}}", System.Text.Encoding.UTF8, "application/json"))
            using (var response = await client.PostAsync(path, content))
            {
                if (!response.IsSuccessStatusCode)
                    throw new Exception($"Server returned code {response.StatusCode}. Body: {await response.Content.ReadAsStringAsync()}");

                return await ReadContentData(response.Content);
            }
        }

        protected async Task<DailyData[]> ReadContentData(HttpContent content)
        {
            using (var stream = await content.ReadAsStreamAsync())
            {
                var data = await JsonSerializer.DeserializeAsync<SourceData>(stream);

                return data.D.Select(d => new DailyData { Date = d.Date, Count = (int)d.Value }).ToArray();
            }
        }

        class SourceJson
        {
            [JsonPropertyName("tiles")]
            public Dictionary<string, SourceTile> Tiles { get; set; }
        }

        class SourceTile
        {
            [JsonPropertyName("id")]
            public int ID { get; set; }
            [JsonPropertyName("name")]
            public string Name { get; set; }
            [JsonPropertyName("url")]
            public string URL { get; set; }
            [JsonPropertyName("updated")]
            public string Updated { get; set; }
            [JsonPropertyName("data")]
            public SourceData Data { get; set; }
        }

        class SourceData
        {
            [JsonPropertyName("d")]
            public SourceEntry[] D { get; set; }
        }

        class SourceEntry
        {
            [JsonPropertyName("t")]
            public string Type { get; set; }
            [JsonPropertyName("v")]
            public double Value { get; set; }
            [JsonPropertyName("d")]
            public string Date { get; set; }
        }
    }

    public class DailyData
    {
        [JsonPropertyName("date")]
        public string Date { get; set; }
        [JsonPropertyName("count")]
        public int Count { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public DateTime ParsedDate { get { return DateTime.ParseExact(Date, "yyMMdd", null); } }
    }

    public class CompleteReport
    {
        [JsonPropertyName("dailyTested")]
        public DailyData[] DailyPositive { get; set; }
        [JsonPropertyName("dailyPositive")]
        public DailyData[] DailyNegative { get; set; }
        [JsonPropertyName("dailyRecoveries")]
        public DailyData[] DailyRecoveries { get; set; }
        [JsonPropertyName("dailyDeaths")]
        public DailyData[] DailyDeaths { get; set; }
    }
}
