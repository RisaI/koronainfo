namespace koronainfo
{
    public interface ISource
    {
        string SourceName { get; }
        string SourceURL { get; }
    }
}