using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using koronainfo.Data;

namespace koronainfo.Services.Scraper.MZCR
{
    public class Scraper : DataSource<CompleteReport>
    {
        public const string Name = "MZČR", URL = "https://onemocneni-aktualne.mzcr.cz/covid-19";
        public const string PersonsURL = "https://onemocneni-aktualne.mzcr.cz/api/v1/covid-19/osoby.csv", RegionCodesPath = "Data/CZnuts.json";
        public const string PRDTURL = "https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakazeni-vyleceni-umrti-testy.csv";

        static readonly Dictionary<string, string> RegionCodes;

        static Scraper()
        {
            RegionCodes = JsonSerializer.Deserialize<Dictionary<string, string>>(System.IO.File.ReadAllBytes(RegionCodesPath));
        }

        public Scraper(DataContainer container)
            : base(container, Name, URL)
        {
            ProvidesCountryData = true;
        }

        protected override IEnumerable<ICountryData> CalculateCountryData(CompleteReport data)
        {
            // Calculate daily data
            var daily = new List<Data.DailyBalance>();
            daily.Add(new Data.DailyBalance() { Date = DateTime.Today, Tested = data.Tested, Cases = data.Cases, Recoveries = data.Recoveries, Deaths = data.Deaths });

            foreach (var day in data.Daily.Where(d => d.ParsedDate < DateTime.Today))
            {
                daily.Add(
                    new Data.DailyBalance {
                        Date = day.ParsedDate,
                        Tested = day.Tested,
                        Cases = day.Cases,
                        Recoveries = day.Recoveries,
                        Deaths = day.Deaths,
                    }
                );
            }

            for (int i = (int)(daily[0].Date - daily[1].Date).TotalDays; i > 1; --i)
            {
                daily.Insert(1, new Data.DailyBalance {
                    Date = DateTime.Today.AddDays(-1), Tested = data.Tested, Cases = data.Cases, Recoveries = data.Recoveries, Deaths = data.Deaths
                });
            }

            // Order and subtract to get deltas
            daily = daily.OrderByDescending(d => d.Date).ToList();
            for (int i = 0; i < daily.Count - 1; ++i)
                daily[i].SetDeltaFrom(daily[i + 1]);

            var czIntel = Container.CountryIntel.First(i => i.ID == "Czech Republic");

            var regional = data.Regional.Select(r => new Data.RegionalBalance() {
                ID = czIntel.Regions.First(rr => rr.DisplayName == r.ID).ID,
                Cases = r.Positive,
            });

            return new [] { new MZCR.CountryData(daily, regional, czIntel) { Source = this } };
        }

        protected override WorldData CalculateWorldData(CompleteReport data)
        {
            return null;
        }

        protected override async Task<(CompleteReport, DateTime)> FetchData()
        {
            var context = BrowsingContext.New(Configuration.Default.WithDefaultLoader());
            var document = await context.OpenAsync(URL);

            var report = new CompleteReport();

            report.Tested = int.Parse(document.GetElementById("count-test").InnerHtml.Replace("&nbsp;", ""));
            report.Cases = int.Parse(document.GetElementById("count-sick").InnerHtml.Replace("&nbsp;", ""));
            report.Deaths = int.Parse(document.GetElementById("count-dead").InnerHtml.Replace(" ", ""));
            report.Recoveries = int.Parse(document.GetElementById("count-recover").InnerHtml.Replace(" ", ""));

            using (var client = new System.Net.Http.HttpClient())
            {
                // Load daily data from the deisgnated MZCR endpoint
                using (var response = await client.GetAsync(PRDTURL))
                {
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    using (var reader = new System.IO.StreamReader(stream))
                    {
                        reader.ReadLine();
                        var daily = new List<DailyData>();

                        while (!reader.EndOfStream)
                        {
                            var line = await reader.ReadLineAsync();

                            if (string.IsNullOrWhiteSpace(line))
                                continue;

                            var split = line.Split(',');
                            daily.Add(new DailyData {
                                Date = DateTime.ParseExact(split[0], "yyyy-MM-dd", null).ToString(MZCR.CountryData.MZCRDateFormat),
                                Tested = int.Parse(split[4]),
                                Cases = int.Parse(split[1]),
                                Recoveries = int.Parse(split[2]),
                                Deaths = int.Parse(split[3])
                            });
                        }

                        report.Daily = daily.OrderByDescending(d => d.ParsedDate).ToArray();
                    }
                }

                using (var response = await client.GetAsync(PersonsURL))
                {
                    using (var stream = await response.Content.ReadAsStreamAsync())
                    using (var reader = new System.IO.StreamReader(stream))
                    {
                        reader.ReadLine();

                        report.Personal = new List<PersonData>();
                        while (!reader.EndOfStream)
                        {
                            var line = await reader.ReadLineAsync();

                            if (string.IsNullOrWhiteSpace(line))
                                continue;

                            report.Personal.Add(PersonData.FromCSVLine(line.Split(',')));
                        }
                    }
                }
            }

            report.Regional = RegionCodes.Keys.Select(k => new RegionalData {
                ID = RegionCodes[k],
                Positive = report.Personal.AsEnumerable().Count(p => p.Region == k)
            }).ToArray();

            return (report, DateTime.Now);
        }

        static RegionalData[] ReadRegional(string data) => JsonSerializer.Deserialize<RegionalData[]>(data);

        public (string, DateTime) GenerateRegionalTimeData()
        {
            var date = DateTime.Now.AddHours(-9).Date;
            var selectedDays = LastFetch.Daily.Where(d => d.ParsedDate < date).Take(4);
            return (JsonSerializer.Serialize(new {
                positive   = selectedDays.Select(d => d.Cases).Reverse(),
                recoveries = selectedDays.Select(d => d.Recoveries).Reverse(),
                deaths     = selectedDays.Select(d => d.Deaths).Reverse(),

                regional = RegionCodes.Keys.Select(k => 
                    (RegionCodes[k], Enumerable.Range(-3, 4).Select(i => 
                        LastFetch.Personal.AsEnumerable().Count(p => p.Region == k && p.Date < date.AddDays(i))
                    ))
                ).ToDictionary(k => k.Item1, v => v.Item2)
            }), date);
        }
    }

    public class DailyData
    {
        [JsonPropertyName("date")]
        public string Date { get; set; }
        [JsonPropertyName("tested")]
        public int Tested { get; set; }
        [JsonPropertyName("cases")]
        public int Cases { get; set; }
        [JsonPropertyName("recoveries")]
        public int Recoveries { get; set; }
        [JsonPropertyName("deaths")]
        public int Deaths { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public DateTime ParsedDate { get { return DateTime.ParseExact(Date, CountryData.MZCRDateFormat, null); } }
    }

    public class CompleteReport
    {
        [JsonPropertyName("tested")]
        public int Tested { get; set; }
        [JsonPropertyName("positive")]
        public int Cases { get; set; }
        [JsonPropertyName("recovered")]
        public int Recoveries { get; set; }
        [JsonPropertyName("deaths")]
        public int Deaths { get; set; }

        [JsonPropertyName("daily")]
        public DailyData[] Daily { get; set; }

        [JsonPropertyName("regional")]
        public RegionalData[] Regional { get; set; }
        [JsonPropertyName("personal")]
        public List<PersonData> Personal { get; set; }
    }

    public class RegionalData
    {
        [JsonPropertyName("name")]
        public string ID { get; set; }
        [JsonPropertyName("value")]
        public int Positive { get; set; }
    }

    public class PersonData
    {
        public DateTime Date { get; set; }
        public int Age { get; set; }
        public bool IsMale { get; set; }
        public string Region { get; set; }
        public string ImportCountry { get; set; }

        public static PersonData FromCSVLine(string[] split)
        {
            return new PersonData {
                Date = DateTime.ParseExact(split[0], "yyyy-MM-dd", null),
                Age = int.Parse(split[1]),
                IsMale = split[2] == "M",
                Region = split[3],
                ImportCountry = string.IsNullOrWhiteSpace(split[5]) ? null : split[5]
            };
        }
    }
}
