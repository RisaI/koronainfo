using System;
using System.Linq;
using System.Collections.Generic;
using koronainfo.Data;

namespace koronainfo.Services.Scraper.MZCR
{
    public class CountryData : ICountryData
    {
        public ISource Source { get; set; }
        public koronainfo.Data.CountryIntel Intel { get; set; }
        public int? Tested { get; private set; }
        public int Cases { get; private set; }
        public int Deaths { get; private set; }
        public int Recoveries { get; private set; }
        public IEnumerable<DailyBalance> Daily { get; private set; }
        public IEnumerable<RegionalBalance> Regional { get; private set; }

        public CountryData(IEnumerable<DailyBalance> daily, IEnumerable<RegionalBalance> regional, Data.CountryIntel intel)
        {
            Intel = intel;
            Daily = daily;
            Regional = regional;

            var current = daily.First();
            Tested = current.Tested;
            Cases = current.Cases; 
            Deaths = current.Deaths;
            Recoveries = current.Recoveries;
        }

        public const string MZCRDateFormat = "d.M.yyyy";
        public static DailyBalance ParseDaily(string line)
        {
            var s = line.Split(';');

            return new DailyBalance() {
                Date = DateTime.ParseExact(s[0], MZCRDateFormat, null),
                Tested = int.Parse(s[1]),
                Cases = int.Parse(s[3]),
            };
        }

        public static DailyBalance ParseDaily(string date, int tested, int cases)
        {
            return new DailyBalance() {
                Date = DateTime.ParseExact(date, MZCRDateFormat, null),
                Tested = tested,
                Cases = cases,
            };
        }
    }
}