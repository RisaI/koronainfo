using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace koronainfo.Services.Scraper
{
    public static class CountryIntel
    {
        public const string CountriesPath = "Data/countries.json";
        
        public static async Task<List<Data.CountryIntel>> ReadCountryIntel()
        {
            using (var stream = new FileStream(CountriesPath, FileMode.Open, FileAccess.Read))
            {
                return await JsonSerializer.DeserializeAsync<List<Data.CountryIntel>>(stream);
            }
        }
    }
}