using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using koronainfo.Data;
using Microsoft.Extensions.Logging;

namespace koronainfo.Services.Scraper.Worldometers
{
    public class Scraper : DataSource<CompleteReport>
    {
        public const string Name = "Worldometers.info", URL = "https://www.worldometers.info/coronavirus/";
        public const string ScriptScraperPath = "scripts/scraper/webscraper.py";
        public const string OutputSeparatorStart = "---";

        public Scraper(DataContainer container)
            : base(container, Name, URL)
        {
            ProvidesCountryData = ProvidesWorldData = true;
        }

        protected override IEnumerable<ICountryData> CalculateCountryData(CompleteReport data)
        {
            var intel = Container.CountryIntel;
            var result = new List<ICountryData>();

            foreach (var country in data.Countries.Select(c => (c, intel.FirstOrDefault(i => i.WorldometersName == c.Country))))
            {
                if (country.Item2 == null)
                    Logger.LogCritical($"Missing Worldometers country '{country.Item1.Country}'.");
                else
                    result.Add(WorldometersCountryData.Parse(this, country.Item1, country.Item2));
            }

            return result;
        }

        protected override WorldData CalculateWorldData(CompleteReport data)
        {
            var currentBalance = new Data.DailyBalance {
                Date = DateTime.Today,
                Cases = data.Countries.Sum(c => c.Cases),
                Deaths = data.Countries.Sum(c => c.Deaths),
                Recoveries = data.Countries.Sum(c => c.Recoveries),
            };

            var daily = data.Daily
                .Select(d => WorldometersCountryData.ParseDaily(this, d))
                .OrderByDescending(d => d.Date)
                .Prepend(currentBalance)
                .ToList();

            for (int i = 0; i < daily.Count - 1; ++i)
                daily[i].SetDeltaFrom(daily[i + 1]);

            return new WorldData(this, daily);
        }

        protected override async Task<(CompleteReport, DateTime)> FetchData()
        {
            var report = new CompleteReport { Countries = new List<CountryLine>(), Daily = new List<DailyLine>() };

            using (var p = SpawnScraperProcess("--wldmeters --wldmeters-daily"))
            {
                await p.WaitForExitAsync();
                if (p.ExitCode > 0)
                    throw new Exception($"Scraper exited with code {p.ExitCode}");

                var stdout = p.StandardOutput;

                // Read country data
                while (!stdout.EndOfStream)
                {
                    var line = await stdout.ReadLineAsync();

                    if (line.StartsWith(OutputSeparatorStart))
                        break;

                    if (line.StartsWith("World"))
                        continue;

                    report.Countries.Add(new CountryLine(line));
                }

                // Read daily world data
                while (!stdout.EndOfStream)
                {
                    var line = await stdout.ReadLineAsync();

                    if (line.StartsWith(OutputSeparatorStart))
                        break;
                    
                    report.Daily.Add(new DailyLine(line));
                }
            }

            return (report, DateTime.Now);
        }

        private static Process SpawnScraperProcess(string args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("python3", $"{ScriptScraperPath} {args}") {
                RedirectStandardOutput = true
            };

            return Process.Start(startInfo);
        }

        public (string, DateTime) GenerateWorldPredictionData()
        {
            var backups = GetAvailableBackups();
            var predictionDates = Enumerable.Range(-5, 5)
                .Select(i => backups.Where(d => d >= DateTime.Today.AddDays(i).AddHours(4)).Min())
                .Select(d => ReadFromFile(d).GetAwaiter().GetResult()).ToArray();

            var countries = predictionDates.First().Countries.Select(c => Container.CountryIntel.FirstOrDefault(i => i.WorldometersName == c.Country)).Where(a => a != null && a.Population.HasValue);

            var data = JsonSerializer.Serialize(countries.ToDictionary(k => k.ID, v => {
                var countryData = predictionDates.Select(d => d.Countries.First(c => c.Country == v.WorldometersName));

                return new {
                    population = v.Population,
                    cases = countryData.Skip(1).Select(c => c.Cases),
                    newcases = Enumerable.Range(0, 4).Select(i => countryData.ElementAt(i + 1).Cases - countryData.ElementAt(i).Cases),
                    deaths = countryData.Skip(1).Select(c => c.Deaths),
                    recoveries = countryData.Skip(1).Select(c => c.Recoveries),
                };
            }));

            return (data, DateTime.Today.AddDays(-1));
        }
    }

    public class CompleteReport
    {
        [JsonPropertyName("countries")]
        public List<CountryLine> Countries { get; set; }
        [JsonPropertyName("daily")]
        public List<DailyLine> Daily { get; set; }
    }

    public class CountryLine
    {
        [JsonPropertyName("country")]
        public string Country { get; set; }
        [JsonPropertyName("cases")]
        public int Cases { get; set; }
        [JsonPropertyName("deaths")]
        public int Deaths { get; set; }
        [JsonPropertyName("recoveries")]
        public int Recoveries { get; set; }

        public CountryLine()
        {

        }

        public CountryLine(string line)
        {
            var s = line.Split(';');
            Country = s[0];
            Cases = int.Parse(s[1]);
            Deaths = int.Parse(s[2]);
            Recoveries = int.Parse(s[3]);
        }
    }

    public class DailyLine
    {
        [JsonPropertyName("date")]
        public string Date { get; set; }
        [JsonPropertyName("cases")]
        public int Cases { get; set; }
        [JsonPropertyName("deaths")]
        public int Deaths { get; set; }
        [JsonPropertyName("recoveries")]
        public int Recoveries { get; set; }

        public DailyLine()
        {

        }

        public DailyLine(string line)
        {
            var s = line.Split(';');
            Date = s[0];
            Cases = int.Parse(s[1]);
            Deaths = int.Parse(s[2]);
            Recoveries = int.Parse(s[3]);
        }
    }
}