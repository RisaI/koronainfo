using System;
using System.Linq;
using System.Collections.Generic;
using koronainfo.Data;

namespace koronainfo.Services.Scraper.Worldometers
{
    public class WorldometersCountryData : Data.ICountryData
    {
        public ISource Source { get; private set; }

        public Data.CountryIntel Intel { get; set; }
        public int? Tested { get; private set; } = null;
        public int Cases { get; private set; }
        public int Deaths { get; private set; }
        public int Recoveries { get; private set; }
        public IEnumerable<DailyBalance> Daily => null;
        public IEnumerable<RegionalBalance> Regional { get { return null; } }

        public string WorldometersCountry { get; private set; }

        private WorldometersCountryData(ISource source)
        {
            Source = source;
        }

        public static WorldometersCountryData Parse(ISource source, CountryLine line, Data.CountryIntel intel)
        {
            return new WorldometersCountryData(source) {
                Intel = intel,
                WorldometersCountry = line.Country,
                Cases = line.Cases,
                Deaths = line.Deaths,
                Recoveries = line.Recoveries
            };
        }

        public static DailyBalance ParseDaily(ISource source, DailyLine line)
        {
            return new DailyBalance {
                Date = DateTime.ParseExact(line.Date, "MMM dd, yyyy", null),
                Cases = line.Cases,
                Deaths = line.Deaths,
                Recoveries = line.Recoveries
            };
        }
    }
}