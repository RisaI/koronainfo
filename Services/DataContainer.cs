using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using koronainfo.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

namespace koronainfo.Services
{
    public class DataContainer
    {
        private List<CountryIntel> _CountryIntel;
        public IEnumerable<CountryIntel> CountryIntel { get { return _CountryIntel; } }

        public DateTime STIRDDate { get; private set; }
        public IEnumerable<STIRDDaily> STIRDCzech { get; private set; }

        public Dictionary<string, List<SIRPDaily>> SIRPWorld { get; set; }
        public Dictionary<string, List<SIRPDaily>> SIRPCzech { get; set; }

        private List<WorldData> _WorldData = new List<WorldData>();
        public IEnumerable<WorldData> WorldData { get { return _WorldData; } }
        private List<ICountryData> _CountryData = new List<ICountryData>();
        public IEnumerable<ICountryData> CountryData { get { return _CountryData; } }

        public ILoggerFactory LoggerFactory { get; private set; }
        public IConfiguration Config { get; private set; }

        public DataContainer(ILoggerFactory loggerFactory, IConfiguration config)
        {
            Config = config;
            LoggerFactory = loggerFactory;
        }

        internal ICountryData GetPrimaryCountryData()
        {
            return _CountryData.First(d => d.Intel.ID == "Czech Republic" && d.Source.SourceName == Scraper.MZCR.Scraper.Name);
        }

        internal IEnumerable<ICountryData> GetAllCountryData()
        {
            return _CountryData.Where(d => d.Source.SourceName == Scraper.Worldometers.Scraper.Name);
        }

        public class Maintainer : IHostedService, IDisposable
        {
            private readonly ILogger _logger;
            private Timer _timer;

            private List<Scraper.IDataSource> Sources = new List<Scraper.IDataSource>();
            public DataContainer Container { get; private set; }

            public Maintainer(DataContainer container)
            {
                Container = container;
                _logger = Container.LoggerFactory.CreateLogger<Maintainer>();
            }

            public async Task StartAsync(CancellationToken cancellationToken)
            {
                _logger.LogInformation("The service is starting");

                _logger.LogDebug("Loading static data");
                
                Container._CountryIntel = await Scraper.CountryIntel.ReadCountryIntel();
                await LoadStird();

                _logger.LogDebug("Setting up data sources");
                Scraper.MZCR.Scraper mzcr;
                Scraper.Worldometers.Scraper wldmeters;
                AddSource(mzcr = new Scraper.MZCR.Scraper(Container)); // MZCR
                AddSource(wldmeters = new Scraper.Worldometers.Scraper(Container)); // Worldometers
                // AddSource(new Scraper.NCZI.Scraper(Container)); // NCZI

                Container._WorldData.AddRange(Sources.Where(s => s.ProvidesWorldData).Select(s => s.WorldData));
                Container._CountryData.AddRange(Sources.Where(s => s.ProvidesCountryData).SelectMany(s => s.CountryData));

                {
                    var (data, date) = mzcr.GenerateRegionalTimeData();
                    Container.SIRPCzech = await Predictions.GetSIRPCzech(data, date);
                    prevSirpCalculation = DateTime.Now;
                }

                {
                    var (data, date) = wldmeters.GenerateWorldPredictionData();
                    Container.SIRPWorld = await Predictions.GetSIRPWorld(data, date);
                    prevSirpWCalculation = DateTime.Now;
                }

                var interval = TimeSpan.FromSeconds(int.Parse(Container.Config["DataMaintainer:RefreshInterval"]));
                _logger.LogDebug($"Setting interval {interval.ToString()}");
                
                _timer = new Timer(OnTick, null, new TimeSpan(Math.Min(15 * TimeSpan.TicksPerSecond, interval.Ticks)), interval);
                ChangeToken.OnChange(() => Container.Config.GetReloadToken(), () => OnConfigurationChanged().Wait());
            }

            FileSystemWatcher stirdWatcher;
            async Task LoadStird()
            {
                _logger.LogDebug("Loading STIRD.");
                
                var file = Container.Config["Predictions:StirdPath"];
                Container.STIRDCzech = (await File.ReadAllLinesAsync(file)).Skip(1).Select(l => STIRDDaily.Parse(l)).ToArray();
                Container.STIRDDate = File.GetLastWriteTime(file);

                if (stirdWatcher != null)
                {
                    stirdWatcher.EnableRaisingEvents = false;
                    stirdWatcher.Path = Path.GetDirectoryName(file);
                    stirdWatcher.Filter = Path.GetFileName(file);
                    stirdWatcher.EnableRaisingEvents = true;
                }
                else
                {
                    stirdWatcher = new FileSystemWatcher() {
                        Path = Path.GetDirectoryName(file),
                        Filter = Path.GetFileName(file),
                        NotifyFilter = NotifyFilters.LastWrite,
                    };

                    stirdWatcher.Changed += (sender, e) => LoadStird().Wait();
                    stirdWatcher.EnableRaisingEvents = true;
                }
            }

            public async Task OnConfigurationChanged()
            {
                await LoadStird();
            }

            DateTime prevSirpCalculation, prevSirpWCalculation;
            public async Task Refresh()
            { 
                var task = Task.WhenAll(Sources.Select(s => s.Fetch(true)).ToArray());

                try
                {
                    await task;
                }
                catch
                {
                    // May do something here is fetching fails
                }

                Container._WorldData.Clear();
                Container._WorldData.AddRange(Sources.Where(s => s.ProvidesWorldData).Select(s => s.WorldData));

                Container._CountryData.Clear();
                Container._CountryData.AddRange(Sources.Where(s => s.ProvidesCountryData).SelectMany(s => s.CountryData));

                {
                    var mzcr = Sources.First(s => s is Scraper.MZCR.Scraper) as Scraper.MZCR.Scraper;

                    if (prevSirpCalculation < mzcr.LastFetchTime)
                    {
                        _logger.LogInformation("MZČR data have changed, recalculating SIRP.");
                        var (data, date) = mzcr.GenerateRegionalTimeData();
                        Container.SIRPCzech = await Predictions.GetSIRPCzech(data, date);
                        prevSirpCalculation = DateTime.Now;
                    }
                }

                {

                    if (prevSirpWCalculation.Date < DateTime.Today && DateTime.Now.Hour > 6)
                    {
                        var wldmeters = Sources.First(s => s is Scraper.Worldometers.Scraper) as Scraper.Worldometers.Scraper;
                        _logger.LogInformation("Recalculating world SIRP.");
                        var (data, date) = wldmeters.GenerateWorldPredictionData();
                        Container.SIRPWorld = await Predictions.GetSIRPWorld(data, date);
                        prevSirpWCalculation = DateTime.Now;
                    }
                }
            }

            private void OnTick(object sender) => Refresh().Wait();

            public Task StopAsync(CancellationToken cancellationToken)
            {
                _logger.LogInformation("Data maintaining service is stopping.");

                _timer?.Change(Timeout.Infinite, 0);

                return Task.CompletedTask;
            }

            public void Dispose()
            {
                _timer?.Dispose();
                Sources.ForEach(s => s.Dispose());
            }

            public void AddSource(Scraper.IDataSource source)
            {
                Sources.Add(source);
                source.InternalChange += OnInternalChange;
            }

            private void OnInternalChange(Scraper.IDataSource source)
            {
                if (source.ProvidesWorldData)
                {
                    Container._WorldData.RemoveAll(d => d.Source == source);
                    Container._WorldData.Add(source.WorldData);
                }

                if (source.ProvidesCountryData)
                {
                    Container._CountryData.RemoveAll(d => d.Source == source);
                    Container._CountryData.AddRange(source.CountryData);
                }
            }

            public class DataMaintainerOptions
            {
                public int RefreshInterval { get; set; } = 600;
            }
        }
    }
}