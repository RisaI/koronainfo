using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using koronainfo.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace koronainfo.Services
{
    public static class Predictions
    {
        const string TimeDataPath = "scripts/timedata_world/";
        const string PredictWorldPath = "scripts/predict_world.jl";
        const string PredictCzechPath = "scripts/predict_czech.jl";

        public static async Task<Dictionary<string, List<SIRPDaily>>> GetSIRPWorld(string data, DateTime firstDate)
        {
            var result = new Dictionary<string, List<SIRPDaily>>();

            using (var julia = SpawnJuliaProcess(PredictWorldPath, "--no-plots"))
            {
                await julia.StandardInput.WriteAsync(data);
                julia.StandardInput.Close();

                var stdout = julia.StandardOutput;
                List<SIRPDaily> currentList = null;

                while (!stdout.EndOfStream)
                {
                    var line = await stdout.ReadLineAsync();

                    if (line.StartsWith('#'))
                    {
                        result.Add(line.Substring(2), currentList = new List<SIRPDaily>());
                    }
                    else
                        currentList.Add(SIRPDaily.Parse(firstDate.AddDays(currentList.Count), line));
                }
            }

            return result;
        }

        public static async Task<Dictionary<string, List<SIRPDaily>>> GetSIRPCzech(string data, DateTime firstDate)
        {
            var result = new Dictionary<string, List<SIRPDaily>>();

            using (var julia = SpawnJuliaProcess(PredictCzechPath, "--no-plots"))
            {
                await julia.StandardInput.WriteAsync(data);
                julia.StandardInput.Close();

                // await julia.WaitForExitAsync();
                // if (julia.ExitCode > 0)
                //     throw new Exception($"Julia exited with code {julia.ExitCode}");

                var stdout = julia.StandardOutput;
                List<SIRPDaily> currentList = null;

                while (!stdout.EndOfStream)
                {
                    var line = await stdout.ReadLineAsync();

                    if (line.StartsWith('#'))
                    {
                        result.Add(line.Substring(2), currentList = new List<SIRPDaily>());
                    }
                    else
                        currentList.Add(SIRPDaily.Parse(firstDate.AddDays(currentList.Count), line));
                }
            }

            return result;
        }

        private static Process SpawnJuliaProcess(string script, string args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("julia", $"{script} {args}") {
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            return Process.Start(startInfo);
        }
    }
}