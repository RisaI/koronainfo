#možné chyby: 
#mzčr neaktualizuje čísla konzistentně ve všech částech najednou, nemusí se shodovat součty
import JSON
using Statistics
using Dates

data = JSON.parse(join(readlines(stdin)))
obyv = JSON.parse(open("Data/kraje_obyv.json"))

Icelk = data["positive"]
Rcelk = data["recoveries"]
Dcelk = data["deaths"]

#vstupy k predikci
timesteps = 7 #kolik kroků se bude predikovat
mortality = 0.026

predIcelk = zeros(timesteps+4)
predRcelk = zeros(timesteps+4)
predDcelk = zeros(timesteps+4)

predIcelkmin = zeros(timesteps+4)
predRcelkmin = zeros(timesteps+4)
predDcelkmin = zeros(timesteps+4)

predIcelkmax = zeros(timesteps+4)
predRcelkmax = zeros(timesteps+4)
predDcelkmax = zeros(timesteps+4)

krajecelkem = sum(r -> r[end], values(data["regional"]))
krajeRatio = Icelk[end] / krajecelkem

for krajName in keys(data["regional"])
    kraj = data["regional"][krajName]
    #print(countryName)

    N = 2/3*obyv[krajName] #2/3 za odhad, že max 2/3 lidí se nakazí

    I = kraj
    Inew = append!([0],kraj[2:end]-kraj[1:end-1])
    R = Rcelk*I[end]/Icelk[end]
    D = Dcelk*I[end]/Icelk[end]

    #predikce
    predI    = Array{Float64}(undef,timesteps+4)
    predR    = Array{Float64}(undef,timesteps+4)
    predInew = Array{Float64}(undef,timesteps+4)
    predD    = Array{Float64}(undef,timesteps+4)
    predI[1:4]    = I[end-3:end]
    predR[1:4]    = R[end-3:end]
    predInew[1:4] = Inew[end-3:end]
    predD[1:4]    = D[end-3:end]

    predImin    = Array{Float64}(undef,timesteps+4)
    predRmin    = Array{Float64}(undef,timesteps+4)
    predInewmin = Array{Float64}(undef,timesteps+4)
    predDmin    = Array{Float64}(undef,timesteps+4)
    predImin[1:4]    = I[end-3:end]
    predRmin[1:4]    = R[end-3:end]
    predInewmin[1:4] = Inew[end-3:end]
    predDmin[1:4]    = D[end-3:end]

    predImax    = Array{Float64}(undef,timesteps+4)
    predRmax    = Array{Float64}(undef,timesteps+4)
    predInewmax = Array{Float64}(undef,timesteps+4)
    predDmax    = Array{Float64}(undef,timesteps+4)
    predImax[1:4]    = I[end-3:end]
    predRmax[1:4]    = R[end-3:end]
    predInewmax[1:4] = Inew[end-3:end]
    predDmax[1:4]    = D[end-3:end]

    global timesteps
        for i in 1:timesteps
            pomer = predInew[i:3+i]./predI[i:3+i]
            gamma = -log.(1 .- pomer)
            S     = N .- predI[i:3+i] .- predR[i:3+i] .- predD[i:3+i]
            beta  = -N *log.(1 .- predInew[i:3+i]./predI[i:3+i]) ./ S
            g    = mean(gamma)
            b    = mean(beta)
            incr = predI[3+i]*(1-exp(-g))

            gSD    = std(gamma) #3*  pro 3σ
            bSD   = std(beta) #3* pro 3σ
            incrmin = incr-predI[3+i]*exp(-g)*gSD#predI[3+i]*(1-exp(-g+gSD))
            incrmax = incr+predI[3+i]*exp(-g)*gSD#predI[3+i]*(1-exp(-g-gSD))
        
            #print("\n",countryName,", i = ",i,", g = ",g)#debug

            predI[i+4] = predI[i+3]+incr
            predInew[i+4] = incr
            predR[i+4] = predR[i+3]+g*predI[i+3]*(1-mortality) 
            predD[i+4] = predD[i+3]+predR[i+4]*mortality

            predImin[i+4] = predImin[i+3]+incrmin
            predInewmin[i+4] = incrmin
            predRmin[i+4] = predRmin[i+3]+(g-gSD)*predImin[i+3]*(1-mortality)
            predDmin[i+4] = predDmin[i+3]+predRmin[i+4]*mortality

            predImax[i+4] = predImax[i+3]+incrmax
            predInewmax[i+4] = incrmax
            predRmax[i+4] = predRmax[i+3]+(g+gSD)*predImax[i+3]*(1-mortality)
            predDmax[i+4] = predDmax[i+3]+predRmax[i+4]*mortality
        end
    #print("\n",predRcelk,"\n")

    println("# $(krajName)");
    for i in 1:timesteps
        println("$(predI[i+4]);$(predImin[i+4]);$(predImax[i+4]);$(predD[i+4]);$(predDmin[i+4]);$(predDmax[i+4]);$(predR[i+4]);$(predRmin[i+4]);$(predRmax[i+4])");
    end

    global predIcelk += predI * krajeRatio;
    global predRcelk += predR * krajeRatio;
    global predDcelk += predD * krajeRatio;

    global predIcelkmin += predImin * krajeRatio
    global predRcelkmin += predRmin * krajeRatio
    global predDcelkmin += predDmin * krajeRatio

    global predIcelkmax += predImax * krajeRatio
    global predRcelkmax += predRmax * krajeRatio
    global predDcelkmax += predDmax * krajeRatio

end

println("# Czech Republic");
for i in 1:timesteps
    println("$(predIcelk[i+4]);$(predIcelkmin[i+4]);$(predIcelkmax[i+4]);$(predDcelk[i+4]);$(predDcelkmin[i+4]);$(predDcelkmax[i+4]);$(predRcelk[i+4]);$(predRcelkmin[i+4]);$(predRcelkmax[i+4])");
end

# for i in 1:timesteps
#     println("$(predIcelk[i+4]);$(predIcelkmin[i+4]);$(predIcelkmax[i+4]);$(predDcelk[i+4]);$(predDcelkmin[i+4]);$(predDcelkmax[i+4]);$(predRcelk[i+4]);$(predRcelkmin[i+4]);$(predRcelkmax[i+4])");
# end


if !("--no-plots" in ARGS)
    using Plots
    pyplot()

    l=length(data["regional"]["Zlínský kraj"])
    scatter()
    for i in keys(data["regional"])
        scatter!(1:l,data["regional"][i],label=i)
    end
    current()

    scatter(0:3,predIcelk[1:4] ,label="",linecolor=:orange)
    scatter!(0:3,predRcelk[1:4],label="",linecolor=:blue)
    scatter!(0:3,predDcelk[1:4],label="",linecolor=:red)

    plot!(0:timesteps+3,predIcelk,label="SIR Poisson - infected",linecolor=:orange)
    plot!(0:timesteps+3,predRcelk,label="SIR Poisson - recovered",linecolor=:blue)
    plot!(0:timesteps+3,predDcelk,label="SIR Poisson - death",linecolor=:red)

    plot!(0:timesteps+3,predIcelkmin,linecolor=:lightgray,label="")
    plot!(0:timesteps+3,predRcelkmin,linecolor=:lightgray,label="")
    plot!(0:timesteps+3,predDcelkmin,linecolor=:lightgray,label="")

    plot!(0:timesteps+3,predIcelkmax,linecolor=:gray,label="")
    plot!(0:timesteps+3,predRcelkmax,linecolor=:gray,label="")
    plot!(0:timesteps+3,predDcelkmax,linecolor=:gray,label="")
end