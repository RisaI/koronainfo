from __future__ import division
from datetime import datetime, timedelta

ticks = 637208094773271365
dt = datetime(1, 1, 1) + timedelta(microseconds=ticks/10)
print(dt.isoformat() + "Z")