#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import urllib.request
from bs4 import BeautifulSoup
import subprocess
import sys
import re
import json

### LIBs
# sudo pip3 install pdfminer
# alebo
# pip3 install --user pdfminer

def re_wrapper(s, subs):
    try:
        ret = re.search(subs, s).group(1)
    except AttributeError:
        ret = ''
    return ret

def print_line(l):
    l = map(lambda s: str(s).replace("\n", ""), l)
    print(SEPARATOR.join(l), end=NEWLINE)

def strip_spaces(s):
    while len(s) > 0 and s[0].isspace():
        s = s[1:]
    while len(s) > 0 and s[-1].isspace():
        s = s[:-1]
    return s

def str2int(s):
    if strip_spaces(s.strip()) == "" or s.strip() == "N/A":
        ret = 0
    else:
        s = s.replace(",", "").replace(" ", "")
        ret = int(s.strip())
    return ret
    
def download_page(url, user_agent=None):
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': user_agent
        }
    )
    with urllib.request.urlopen(req, timeout=30) as response:
        html = response.read()
    return html # .decode('utf-8')

def download_file(url, filename="tmp/document.pdf", user_agent=None, attempts=5):
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': user_agent
        }
    )
    with urllib.request.urlopen(req, timeout=30) as response:
        file = open("pdf/document.pdf", 'wb')
        file.write(response.read())
        file.close()
        html = response.read()
    return filename

def table2arr(table, sel_col="all", sel_row="all"):
    result = []
    allrows = table.findAll('tr')
    for row_i in range(len(allrows)):
        if sel_row != "all" and row_i not in sel_row:
            continue
        row = allrows[row_i]

        # Should be moved
        if row.has_attr('class'):
            if 'total_row_world' in row['class']:
                continue

        result.append([])
        allcols = row.findAll('td')
        for col_i in range(len(allcols)):
            if sel_col != "all" and col_i not in sel_col:
                continue
            col = allcols[col_i]
            thestrings = [s for s in col.findAll(text=True)]
            thetext = ''.join(thestrings)
            result[-1].append(thetext)
    return result

def head2arr(head, sel_col="all"):
    result = []
    allcols = head.findAll('th')
    for col_i in range(len(allcols)):
        if sel_col != "all" and col_i not in sel_col:
            continue
        col = allcols[col_i]
        thestrings = [s for s in col.findAll(text=True)]
        thetext = strip_spaces(' '.join(thestrings))
        result.append(thetext)
    return result

def arr2json(table_arr, header=None, info=None):
    header_len = len(header)
    table_dic = []

    if table_arr != [] and header == None:
        raise ValueError('Gimme! Gimme! Gimme some header!')

    for row_i in range(len(table_arr)):
        row = table_arr[row_i]
        row_dic = {}
        if len(row) != header_len:
            print(row)
            print(header)
            raise ValueError('Row size is not the same as header size!')
        for i in range(header_len):
            row_dic[header[i]] = row[i]
        if info != None and info[row_i][i] != "":
            row_dic['info('+str(i+1)+')'] = info[row_i][i]
        table_dic.append(row_dic)
 
    return json.dumps(table_dic, ensure_ascii=False, indent=4)

def single_output(t_body, t_header=None, t_info=None):
    if json_output:
        print(arr2json(t_body, header=t_header, info=t_info))
    else:
        if t_header != None and header:
            print_line(t_header)
        for row in t_body:
            print_line(row)
        if t_info != None and info:
            print(SRC_SEPAR)
            for row in table_info:
                print_line(row)
    print(SRC_SEPAR)

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.131 Safari/537.36'

NEWLINE = "\n"
SEPARATOR = ";"
SRC_SEPAR = "-"*80

# What sources to scrape
args = list(map(lambda s: s.lower(), sys.argv))
fetch_worldometers = "--wldmeters" in args
fetch_worldometers_daily = "--wldmeters-daily" in args
fetch_mzcr_karantena = "--mzcr-karantena" in args
fetch_ecdc_cases = "--ecdc-cases" in args
fetch_ecdc_transmission = "--ecdc-transmission" in args
ct24 = "--ct24" in args
if "--pages" in args:
    pages = int(args[args.index("--pages")+1])
header = "--header" in args
info = "--info" in args
json_output = "--json" in args

soup = None


if fetch_worldometers:
    url = "https://www.worldometers.info/coronavirus/"
    html = download_page(url, user_agent=USER_AGENT)

    soup = BeautifulSoup(html, "lxml")
    table = soup.findAll("table")
    table_header = table[0].find("thead")
    table_body = table[0].find("tbody")
    
    col_option = [1, 2, 4, 6] # country, cases, deaths, recoveries
    header_l = head2arr(table_header, sel_col=col_option)
    table_l = table2arr(table_body, sel_col=col_option) 
    header_l = list(map(lambda s: s.replace("\n", ""), header_l))
    table_l = list(filter(lambda x: x != [], table_l))

    table_conv = []
    for row in table_l:
        if row != []:
            row[0] = strip_spaces(row[0])
            for i in range(1, len(row)):
                row[i] = str2int(row[i])

    single_output(table_l, t_header=header_l)


if fetch_worldometers_daily:
    if not fetch_worldometers:
        url = "https://www.worldometers.info/coronavirus/"
        html = download_page(url, user_agent=USER_AGENT)
        soup = BeautifulSoup(html, "lxml")

    scripts = soup.find_all("script") # , attrs={"type": "text/javascript"}
    data_js = {}
    data_len = 0
    for i, scr in enumerate(scripts):
        if "coronavirus-cases-linear" in str(scr.string):
            match = re_wrapper(scr.string, " categories: [^[]*\[([^]]*)\]")
            data_js["dates"] = match.strip("\"").split("\",\"")
            match = re_wrapper(scr.string, "name: 'Cases'[^[]*\[([^]]*)\]")
            data_js["cases"] = list(map(int, match.split(",")))

        elif "coronavirus-deaths-linear" in str(scr.string):
            match = re_wrapper(scr.string, "name: 'Deaths'[^[]*\[([^]]*)\]")
            data_js["deaths"] = list(map(int, match.split(",")))
            data_len = len(data_js["deaths"])

    url = "https://www.worldometers.info/coronavirus/coronavirus-cases"
    html = download_page(url, user_agent=USER_AGENT)
    soup = BeautifulSoup(html, "lxml")

    scripts = soup.findAll("script") # , attrs={"type": "text/javascript"}

    for i, scr in enumerate(scripts):
        if "graph-cured-total" in str(scr.string):
            match = re_wrapper(scr.string, "name: 'Cured'[^[]*\[([^]]*)\]")
            data_js["recoveries"] = list(map(int, match.split(",")))

    recovery_lag = len(data_js["deaths"]) - len(data_js["recoveries"])
    for data_i in range(data_len):
        row = [
            data_js["dates"][data_i],
            data_js["cases"][data_i],
            data_js["deaths"][data_i],
            "0" if data_i < recovery_lag else data_js["recoveries"][data_i-recovery_lag]
        ]
        print_line(row)
    print(SRC_SEPAR)

if fetch_ecdc_cases:
    url = "https://www.ecdc.europa.eu/en/cases-2019-ncov-eueea"
    html = download_page(url, user_agent=USER_AGENT)

    soup = BeautifulSoup(html, "lxml")
    table = soup.findAll("table") 

    table_l = table2arr(table[0])
    table_header = table[0].find("thead")
    table_body = table[0].find("tbody")
    
    col_option = "all"
    header_l = head2arr(table_header, sel_col=col_option)
    table_l = table2arr(table_body, sel_col=col_option) 
    header_l = list(map(lambda s: s.replace("\n", ""), header_l))
    table_l = list(filter(lambda x: x != [], table_l))

    for row in table_l:
        if row != []:
            row[0] = strip_spaces(row[0]).strip()
            for i in range(1, len(row)):
                row[i] = str2int(row[i])

    single_output(table_l, t_header=header_l)

# Je stale problematicke vybrat iba ziadane divka
# Dopln stahovanie pomocou wget
if False:
    url = "https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports"

    pdf_name = "20200128-sitrep-8-ncov-cleared.pdf"

    cmd = "pdf2txt.py -t html pdf/20200128-sitrep-8-ncov-cleared.pdf"
    result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE)
    html = result.stdout
    soup = BeautifulSoup(html, "lxml")
    stops = [
        '<span style="font-family: BCDFEE+Calibri-Bold; font-size:11px">South-East Asia',
        '<span style="font-family: BCDFEE+Calibri-Bold; font-size:11px">Total Confirmed cases'
    ]
    stops_i = [-1, -1]
    divs = soup.findAll("span", attrs={"style": "font-family: BCDEEE+Calibri; font-size:11px"})
    for div_i in range(len(divs)):
        print("South-East Asia" in divs[div_i])
        print(stops[0])
        print(divs[div_i])
        if stops[0] in divs[div_i]:
            stops_i[0] = div_i
        if stops[1] in divs[div_i]:
            stops_i[1] = div_i

    divs_table = divs[stops_i[0]+1:stops_i[1]]

    for d in divs_table:
        print(d)
    print(len(divs))
    print(len(divs_table))
    print(stops_i)