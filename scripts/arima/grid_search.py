# pip install statsmodels sklearn numpy matplotlib pandas statistics
'''
ARIMA(p,d,q)(P, D, Q)m,

    p — the number of autoregressive
    d — degree of differencing
    q — the number of moving average terms
    m — refers to the number of periods in each season
    (P, D, Q )— represents the (p,d,q) for the seasonal part of the time series
'''

import sys
import warnings
from multiprocessing import Pool

import numpy as np
from math import sqrt
from sklearn.metrics import mean_squared_error
import pandas as pd

import statsmodels
import statistics
from statsmodels.tsa.arima_model import ARIMA
import matplotlib.pyplot as plt

from tqdm import tqdm

# ARIMA will not converge in many cases 
if not sys.warnoptions:
    warnings.simplefilter("ignore")

def download_csv(url):
    df = pd.read_csv(url, error_bad_lines=False)
    header, data = df.columns.tolist(), df.values.tolist()
    data = [[x for x in row if str(x) != 'nan'] for row in data]
    return [header, data]

# Evaluate ARIMA
def evaluate_arima_model(args):
    ratio = 0.66
    seq, arima_order = args

    # prepare training dataset
    seq = np.array(seq, dtype='float64')
    train_size = int(len(seq) * ratio)
    train, test = seq[0:train_size], seq[train_size:]
    history = [x for x in train]

    # make predictions
    predictions = list()
    for t in range(len(test)):
        try:
            model = ARIMA(history, order=arima_order)
            model_fit = model.fit(disp=0, solver='lbfgs')
            yhat = model_fit.forecast()[0]
            predictions.append(yhat)
            history.append(test[t])
            if yhat != yhat:
                return 1e10, arima_order
        except:
            return 1e10, arima_order

    # calculate out of sample error
    error = mean_squared_error(test, predictions)
    return error, arima_order

# Run ARIMA on all selected sequences at once for p, d, q
def run_arima_parallel(data, min_order=(0, 0, 0), max_order=(5, 5, 5), n_jobs=4):
    sequences = []
    for seq in data:
        seq = list(filter(lambda x: x != 0, seq[4:]))
        if sum(seq) > ppl_cutoff:
            sequences.append(seq)
    arima_res_mean = np.zeros(max_order, dtype='float64')
    arima_res_std = np.zeros(max_order, dtype='float64')

    pool = Pool(processes=n_jobs)
    for p in tqdm(range(min_order[0], max_order[0])):
        for d in tqdm(range(min_order[1], max_order[1])):
            for q in tqdm(range(min_order[2], max_order[2])):
                arima_err = []
                args_l = []
                for seq_i in range(len(sequences)):
                    seq = sequences[seq_i]
                    args_l.append([seq, (p, d, q)])

                result = pool.map(evaluate_arima_model, args_l)

                for res in result:
                    if res[0] < 1e9:
                        arima_err.append(res[0])
                if len(arima_err) >= 3:
                    arima_res_mean[p,d,q] = statistics.mean(arima_err)
                    arima_res_std[p,d,q] = statistics.stdev(arima_err)
                else:
                    arima_res_mean[p,d,q] = 1e5
                    arima_res_std[p,d,q] = 1e5
    
    return arima_res_mean, arima_res_std

# Download data from github

confirmed_h, confirmed = download_csv(
    'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv')
death_h, death = download_csv(
    'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv')
recover_h, recover = download_csv(
    'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Recovered.csv')

# we will use ARIMA models for C, D, R separately

args = list(map(lambda s: s.lower(), sys.argv))
run_confirmed = "--confirmed" in args
run_deaths = "--deaths" in args
run_recovered = "--recovered" in args
if "--cutoff" in args:
    ppl_cutoff = float(args[args.index("--cutoff")+1])

# Test grid parameters on C, D or R

if run_confirmed: 
    arima_res_mean, arima_res_std = run_arima_parallel(
        confirmed, min_order=(0, 1, 0), max_order=(5, 2, 6), n_jobs=10
        )
    min_ind = np.unravel_index(np.argmin(arima_res_mean, axis=None), arima_res_mean.shape)
    p, d, q = min_ind

    with np.printoptions(precision=2, suppress=True):
        print(arima_res_mean[:,1,:])
        print(arima_res_std[:,1,:])

if run_deaths:
    arima_res_mean, arima_res_std = run_arima_parallel(
        death, min_order=(0, 1, 0), max_order=(5, 2, 6), n_jobs=10
        )
    min_ind = np.unravel_index(np.argmin(arima_res_mean, axis=None), arima_res_mean.shape)
    p, d, q = min_ind

    with np.printoptions(precision=2, suppress=True):
        print(arima_res_mean[:,1,:])
        print(arima_res_std[:,1,:])

if run_recovered:
    arima_res_mean, arima_res_std = run_arima_parallel(
        recover, min_order=(0, 1, 0), max_order=(5, 2, 6), n_jobs=10
        )
    min_ind = np.unravel_index(np.argmin(arima_res_mean, axis=None), arima_res_mean.shape)
    p, d, q = min_ind

    with np.printoptions(precision=2, suppress=True):
        print(arima_res_mean[:,1,:])
        print(arima_res_std[:,1,:])