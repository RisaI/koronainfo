# pip install statsmodels sklearn numpy matplotlib pandas statistics 
''' SARIMA
ARIMA(p,d,q)(P, D, Q)m,

    p — the number of autoregressive
    d — degree of differencing
    q — the number of moving average terms
    m — refers to the number of periods in each season
    (P, D, Q )— represents the (p,d,q) for the seasonal part of the time series
'''

import sys
import warnings

import urllib.request
from bs4 import BeautifulSoup
import re

import numpy as np
from sklearn.metrics import mean_squared_error
import pandas as pd
import statsmodels
import statistics
from statsmodels.tsa.arima_model import ARIMA

# ARIMA will not converge in many cases 
# if not sys.warnoptions:
#     warnings.simplefilter("ignore")

def re_wrapper(s, subs):
    try:
        ret = re.search(subs, s).group(1)
    except AttributeError:
        ret = ''
    return ret

def print_line(l):
    l = map(lambda s: str(s).replace("\n", ""), l)
    print(SEPARATOR.join(l), end=NEWLINE)

def download_page(url, user_agent=None):
    req = urllib.request.Request(
        url, 
        data=None, 
        headers={
            'User-Agent': user_agent
        }
    )
    with urllib.request.urlopen(req, timeout=30) as response:
        html = response.read()
    return html # .decode('utf-8')

def download_data():
    url = "https://www.worldometers.info/coronavirus/"
    html = download_page(url, user_agent=USER_AGENT)
    soup = BeautifulSoup(html, "lxml")

    scripts = soup.findAll("script") # , attrs={"type": "text/javascript"}
    data_js = {}
    data_len = 0
    for i, scr in enumerate(scripts):
        if "coronavirus-cases-linear" in str(scr.text):
            match = re_wrapper(scr.text, " categories: [^[]*\[([^]]*)\]")
            data_js["dates"] = match.replace('"', '').split(",")
            match = re_wrapper(scr.text, "name: 'Cases'[^[]*\[([^]]*)\]")
            data_js["cases"] = list(map(int, match.split(",")))

        elif "coronavirus-deaths-linear" in str(scr.text):
            match = re_wrapper(scr.text, "name: 'Deaths'[^[]*\[([^]]*)\]")
            data_js["deaths"] = list(map(int, match.split(",")))
            data_len = len(data_js["deaths"])

    url = "https://www.worldometers.info/coronavirus/coronavirus-cases"
    html = download_page(url, user_agent=USER_AGENT)
    soup = BeautifulSoup(html, "lxml")

    scripts = soup.findAll("script") # , attrs={"type": "text/javascript"}

    for i, scr in enumerate(scripts):
        if "graph-cured-total" in str(scr.text):
            match = re_wrapper(scr.text, "name: 'Cured'[^[]*\[([^]]*)\]")
            data_js["recoveries"] = list(map(int, match.split(",")))

    data = []
    recovery_lag = len(data_js["deaths"]) - len(data_js["recoveries"])
    for data_i in range(data_len):
        row = [
            data_js["dates"][data_i],
            data_js["cases"][data_i],
            data_js["deaths"][data_i],
            "0" if data_i < recovery_lag else data_js["recoveries"][data_i-recovery_lag]
        ]
        data.append(row)
    
    return data

NEWLINE = "\n"
SEPARATOR = ";"
SRC_SEPAR = "#"*80

args = list(map(lambda s: s.lower(), sys.argv))
days_forecats = 4
if "--predict" in args:
    days_forecats = int(args[args.index("--predict")+1])

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:73.0) Gecko/20100101 Firefox/73.0'

data = download_data()

dates, confirmed, deaths, recovered = [], [], [], []
for row in data:
    row[1], row[2], row[3] = int(row[1]), int(row[2]), int(row[3])
    dates.append(row[0])
    confirmed.append(row[1])
    deaths.append(row[2])
    recovered.append(row[3])

arima_order = [(6, 1, 1), (5, 1, 1), (5, 1, 1)]

forecast = np.zeros((days_forecats, 12))

arrs = [confirmed, deaths, recovered]
arrs_names = ["confirmed", "deaths", "recovered"]
for arr_i in [0, 1, 2]:
    try:
        model = ARIMA(arrs[arr_i], order=arima_order[0])
        model_fit = model.fit(solver='lbfgs')
        pred, stderr, ci = model_fit.forecast(days_forecats)
    except:
        raise Exception('Error: ARIMA went into some trouble.')

    print(model_fit.summary())

    for day_i in range(days_forecats):
        forecast[day_i][arr_i*4] = pred[day_i]
        forecast[day_i][arr_i*4+1] = stderr[day_i]
        forecast[day_i][arr_i*4+2] = ci[day_i][0]
        forecast[day_i][arr_i*4+3] = ci[day_i][1]

print(SRC_SEPAR)
header = [
    "confirmed_mean", "confirmed_std", "confirmed_ci_low", "confirmed_ci_high", 
    "deaths_mean", "deaths_std", "deaths_ci_low", "deaths_ci_high", 
    "recovered_mean", "recovered_std", "recovered_ci_low", "recovered_ci_high", 
]
print_line(header)
for day_row in forecast:
    print_line(day_row)