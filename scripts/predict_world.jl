import JSON
using Statistics
using Dates

data = JSON.parse(join(readlines(stdin)))

timesteps = 7 #kolik kroků se bude predikovat
totalinf = 0
mortality = 0.034

predIcelk = zeros(timesteps+4)
predRcelk = zeros(timesteps+4)
predDcelk = zeros(timesteps+4)

predIcelkmin = zeros(timesteps+4)
predRcelkmin = zeros(timesteps+4)
predDcelkmin = zeros(timesteps+4)

predIcelkmax = zeros(timesteps+4)
predRcelkmax = zeros(timesteps+4)
predDcelkmax = zeros(timesteps+4)

for countryName in keys(data)
    country = data[countryName]

    #data pro každý stát
    N = country["population"] #2/3 za odhad, že max 2/3 lidí se nakazí

    I = country["cases"]
    Inew = country["newcases"]
    R = country["recoveries"]
    D = country["deaths"]

    #predikce
    predI    = Array{Float64}(undef,timesteps+4)
    predR    = Array{Float64}(undef,timesteps+4)
    predInew = Array{Float64}(undef,timesteps+4)
    predD    = Array{Float64}(undef,timesteps+4)
    predI[1:4]    = I[end-3:end]
    predR[1:4]    = R[end-3:end]
    predInew[1:4] = Inew[end-3:end]
    predD[1:4]    = D[end-3:end]

    predImin    = Array{Float64}(undef,timesteps+4)
    predRmin    = Array{Float64}(undef,timesteps+4)
    predInewmin = Array{Float64}(undef,timesteps+4)
    predDmin    = Array{Float64}(undef,timesteps+4)
    predImin[1:4]    = I[end-3:end]
    predRmin[1:4]    = R[end-3:end]
    predInewmin[1:4] = Inew[end-3:end]
    predDmin[1:4]    = D[end-3:end]

    predImax    = Array{Float64}(undef,timesteps+4)
    predRmax    = Array{Float64}(undef,timesteps+4)
    predInewmax = Array{Float64}(undef,timesteps+4)
    predDmax    = Array{Float64}(undef,timesteps+4)
    predImax[1:4]    = I[end-3:end]
    predRmax[1:4]    = R[end-3:end]
    predInewmax[1:4] = Inew[end-3:end]
    predDmax[1:4]    = D[end-3:end]

    global timesteps
    for i in 1:timesteps
        if minimum(predI[i:3+i])>0.1 && sum(predI[i:3+i].>1e9)==0
            pomer = predInew[i:3+i]./predI[i:3+i]
            if sum(pomer.==1.0)!=0
                pomer = predInew[1+i:3+i]./predI[1+i:3+i]
            end
            if sum(pomer.==1.0)!=0
                pomer = predInew[2+i:3+i]./predI[2+i:3+i]
            end
            if sum(pomer.==1.0)!=0
                pomer = predInew[3+i:3+i]./predI[3+i:3+i]
            end
            if sum(pomer.==1.0)!=0
                pomer = 0
            end
            gamma = -log.(1 .- pomer)
            S     = N .- predI[i:3+i] .- predR[i:3+i] .- predD[i:3+i]
            beta  = -N *log.(1 .- predInew[i:3+i]./predI[i:3+i]) ./ S
            g    = mean(gamma)
            b    = mean(beta)
            incr = predI[3+i]*(1-exp(-g))

            gSD    = std(gamma) #3*  pro 3σ
            bSD   = std(beta) #3* pro 3σ
            incrmin = incr-predI[3+i]*exp(-g)*gSD#predI[3+i]*(1-exp(-g+gSD))
            incrmax = incr+predI[3+i]*exp(-g)*gSD#predI[3+i]*(1-exp(-g-gSD))
        else
            g    = 0
            incr = 0
            gamma = 0

            gSD    = 0
            bSD   = 0
            incrmin = 0
            incrmax = 0
        end
        #print("\n",countryName,", i = ",i,", g = ",g)#debug

        predI[i+4] = predI[i+3]+incr
        predInew[i+4] = incr
        predR[i+4] = predR[i+3]+g*predInew[i+3]*(1-mortality) 
        predD[i+4] = predD[i+3]+predR[i+4]*mortality

        predImin[i+4] = predImin[i+3]+incrmin
        predInewmin[i+4] = incrmin
        predRmin[i+4] = predRmin[i+3]+abs((g-gSD)*predImin[i+3])*(1-mortality) 
        predDmin[i+4] = predDmin[i+3]+predRmin[i+4]*mortality

        predImax[i+4] = predImax[i+3]+incrmax
        predInewmax[i+4] = incrmax
        predRmax[i+4] = predRmax[i+3]+abs((g+gSD)*predImax[i+3])*(1-mortality)
        predDmax[i+4] = predDmax[i+3]+predRmax[i+4]*mortality
    end
    #print("\n",predRcelk,"\n")

    println("# $(countryName)")
    for i in 1:timesteps
        println("$(predI[i+4]);$(predImin[i+4]);$(predImax[i+4]);$(predD[i+4]);$(predDmin[i+4]);$(predDmax[i+4]);$(predR[i+4]);$(predRmin[i+4]);$(predRmax[i+4])");
    end

    global predIcelk += predI
    global predRcelk += predR
    global predDcelk += predD

    global predIcelkmin += predImin
    global predRcelkmin += predRmin
    global predDcelkmin += predDmin

    global predIcelkmax += predImax
    global predRcelkmax += predRmax
    global predDcelkmax += predDmax
end

println("# Global")
for i in 1:timesteps
    println("$(predIcelk[i+4]);$(predIcelkmin[i+4]);$(predIcelkmax[i+4]);$(predDcelk[i+4]);$(predDcelkmin[i+4]);$(predDcelkmax[i+4]);$(predRcelk[i+4]);$(predRcelkmin[i+4]);$(predRcelkmax[i+4])");
end

# print(predIcelk,"\n")
# print(predRcelk,"\n")
# print(predDcelk,"\n")

# print(predIcelkmin,"\n")
# print(predRcelkmin,"\n")
# print(predDcelkmin,"\n")

# print(predIcelkmax,"\n")
# print(predRcelkmax,"\n")
# print(predDcelkmax,"\n")


if !("--no-plots" in ARGS)
    using Plots
    pyplot()

    scatter(0:4,predIcelk[1:4] ,label="",linecolor=:orange)
    scatter!(0:4,predRcelk[1:4],label="",linecolor=:blue)
    scatter!(0:4,predDcelk[1:4],label="",linecolor=:red)

    plot!(0:timesteps+3,predIcelk,label="SIR Poisson - infected",linecolor=:orange)
    plot!(0:timesteps+3,predRcelk,label="SIR Poisson - recovered",linecolor=:blue)
    plot!(0:timesteps+3,predDcelk,label="SIR Poisson - death",linecolor=:red)

    plot!(0:timesteps+3,predIcelkmin,linecolor=:lightgray,label="")
    plot!(0:timesteps+3,predRcelkmin,linecolor=:lightgray,label="")
    plot!(0:timesteps+3,predDcelkmin,linecolor=:lightgray,label="")

    plot!(0:timesteps+3,predIcelkmax,linecolor=:gray,label="")
    plot!(0:timesteps+3,predRcelkmax,linecolor=:gray,label="")
    plot!(0:timesteps+3,predDcelkmax,linecolor=:gray,label="")
end