using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace koronainfo
{
    public static class Extensions
    {
        /// <summary>
        /// A helper method to make Process.WaitForExit async.
        /// </summary>
        /// <param name="process"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static Task WaitForExitAsync(this Process process, 
            CancellationToken cancellationToken = default(CancellationToken))
        {
            if (process.HasExited)
                return Task.CompletedTask;
                
            var tcs = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => tcs.TrySetResult(null);
            if(cancellationToken != default(CancellationToken))
                cancellationToken.Register(tcs.SetCanceled);

            return tcs.Task;
        }

        public static System.Globalization.CultureInfo CzechCulture { get { return System.Globalization.CultureInfo.GetCultureInfo("cs-CZ"); } }

        public static string Pretty(this int num)
        {
            return num.ToString("n0", CzechCulture);
        }
        public static string Pretty(this float num)
        {
            return num.ToString("n2", CzechCulture);
        }
        public static string Pretty(this float num, int decPlaces)
        {
            return num.ToString($"n{decPlaces}", CzechCulture);
        }
        public static string Plotly(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }
    }
}