using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace koronainfo.Pages
{
    public class DebugModel : PageModel
    {
        private readonly ILogger<DebugModel> _logger;
        private readonly Services.DataContainer _data;

        public IEnumerable<Data.ICountryData> Data;

        public DebugModel(ILogger<DebugModel> logger, Services.DataContainer data)
        {
            _logger = logger;
            _data = data;
        }

        public Task OnGetAsync()
        {
            var intel = _data.CountryIntel;
            Data = _data.GetAllCountryData();

            return Task.CompletedTask;
        }
    }
}
