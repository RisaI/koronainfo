using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace koronainfo.Components
{
    public class WorldWidgetViewComponent : ViewComponent
    {
        public WorldWidgetViewComponent()
        {

        }

        public Task<IViewComponentResult> InvokeAsync(Data.WorldData data)
        {
            return Task.FromResult<IViewComponentResult>(View(data)); 
        }
    }
}