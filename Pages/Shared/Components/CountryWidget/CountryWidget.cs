using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace koronainfo.Components
{
    public class CountryWidgetViewComponent : ViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(Data.ICountryData data, string className, string id)
        {
            ViewData["class"] = className;
            ViewData["id"] = id ?? data.Intel.ID.Replace(" ", "-").ToLower();
            return Task.FromResult<IViewComponentResult>(View(data)); 
        }
    }
}