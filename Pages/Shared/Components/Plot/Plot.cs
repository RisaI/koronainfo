using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace koronainfo.Components
{
    public class PlotViewComponent : ViewComponent
    {
        public PlotViewComponent()
        {
            
        }

        public Task<IViewComponentResult> InvokeAsync(Plot plot)
        {
            return Task.FromResult<IViewComponentResult>(View(plot)); 
        }
    }

    public class Plot
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public IEnumerable<PlotData> Data { get; set; }
        public string XAxis { get; set; }
        public string YAxis { get; set; }

        public int Height { get; set; } = 450;
        public int TraceGroupGap { get; set; } = 10;

        public (object, object)? XRange { get; set; }
        public (object, object)? YRange { get; set; }
    }

    public class PlotData
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonIgnore]
        public DataMode Mode { get; set; } = DataMode.LinesMarkers;
        [JsonPropertyName("line")]
        public PlotLine Line { get; set; } = new PlotLine();
        [JsonPropertyName("marker")]
        public PlotMarker Marker { get; set; } = new PlotMarker();
        [JsonPropertyName("x")]
        public IEnumerable<object> X { get; set; }
        [JsonPropertyName("y")]
        public IEnumerable<object> Y { get; set; }
        [JsonPropertyName("text")]
        public IEnumerable<string> Texts { get; set; }

        [JsonPropertyName("showlegend")]
        public bool ShowLegend { get; set; } = true;
        [JsonPropertyName("legendgroup")]
        public string LegendGroup { get; set; } = string.Empty;

        [JsonPropertyName("hoverinfo")]
        public string HoverInfo { get; set; } = "all";

        [JsonPropertyName("fill")]
        public string FillName
        {
            get { return Enum.GetName(typeof(FillMode), Fill).ToLower(); }
        }
        [JsonIgnore]
        public FillMode Fill { get; set; } = FillMode.None;
        [JsonPropertyName("fillcolor")]
        public string FillColor { get; set; }
        [JsonPropertyName("mode")]
        public string ModeString
        {
            get {
                switch (Mode)
                {
                    case DataMode.Lines:
                        return "lines";
                    case DataMode.Markers:
                        return "markers";
                    case DataMode.LinesMarkers:
                    default:
                        return "lines+markers";
                }
            }
        }

        public enum DataMode
        {
            LinesMarkers,
            Lines,
            Markers,
        }

        public enum FillMode
        {
            None,
            ToZeroY,
            ToNextY,
            ToSelf,
        }
    }

    public class PlotLine
    {
        [JsonPropertyName("width")]
        public int Width { get; set; } = 2;
        [JsonPropertyName("color")]
        public string Color { get; set; }
        [JsonPropertyName("dash")]
        public string DashName
        {
            get { return Enum.GetName(typeof(DashMode), Dash).ToLower(); }
        }
        [JsonPropertyName("shape")]
        public string ShapeName
        {
            get { return Enum.GetName(typeof(ShapeMode), Shape).ToLower(); }
        }

        [JsonIgnore]
        public DashMode Dash { get; set; }
        [JsonIgnore]
        public ShapeMode Shape { get; set; } = ShapeMode.Linear;

        public enum DashMode
        {
            Solid,
            Dot,
            DashDot
        }

        public enum ShapeMode
        {
            Linear,
            Hv,
            Vh,
            Hvh,
            Vhv,
            Spline,
        }
    }

    public class PlotMarker
    {
        [JsonPropertyName("size")]
        public int Size { get; set; } = 6;
        [JsonPropertyName("color")]
        public string Color { get; set; }
        [JsonPropertyName("line")]
        public PlotLine Line { get; set; }
    }
}