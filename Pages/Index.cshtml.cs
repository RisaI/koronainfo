﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace koronainfo.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly Services.DataContainer _data;
        private readonly IConfiguration _config;

        public IndexModel(ILogger<IndexModel> logger, Services.DataContainer data, IConfiguration config)
        {
            _logger = logger;
            _data = data;
            _config = config;
        }

        public Data.ICountryData CzechData;
        // public Data.ICountryData SlovakianData;
        public IEnumerable<Data.ICountryData> GlobalData;
        public Data.WorldData WorldData;
        public string DangerNotice { get { return _config["Notice:Danger"]; } }
        public string WarningNotice { get { return _config["Notice:Warning"]; } }
        public string InfoNotice { get { return _config["Notice:Info"]; } }

        public Task OnGetAsync()
        {
            CzechData = _data.GetPrimaryCountryData();
            // SlovakianData = _data.CountryData.First(c => c.Intel.ID == "Slovakia" && c.Source.SourceName == Services.Scraper.NCZI.Scraper.Name);
            GlobalData = _data.GetAllCountryData();
            WorldData = _data.WorldData.First(d => d.Source.SourceName == Services.Scraper.Worldometers.Scraper.Name);

            return Task.CompletedTask;
        }
    }
}
