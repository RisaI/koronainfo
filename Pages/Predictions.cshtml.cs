using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace koronainfo.Pages
{
    public class PredictionsModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly Services.DataContainer _data;

        public PredictionsModel(ILogger<IndexModel> logger, Services.DataContainer data)
        {
            _logger = logger;
            _data = data;
        }

        public DateTime STIRDDate;
        public IEnumerable<Data.STIRDDaily> STIRDCzech;
        public IEnumerable<Data.SIRPDaily> SIRPWorld;
        public Dictionary<string, List<Data.SIRPDaily>> SIRPCzech;
        public Data.ICountryData PrimaryCountry;
        public Data.WorldData WorldData;

        public Task OnGetAsync()
        {
            STIRDCzech = _data.STIRDCzech;
            STIRDDate = _data.STIRDDate;
            SIRPWorld = _data.SIRPWorld["Global"];
            SIRPCzech = _data.SIRPCzech;
            PrimaryCountry = _data.GetPrimaryCountryData();
            WorldData = _data.WorldData.First(d => d.Source.SourceName == Services.Scraper.Worldometers.Scraper.Name);

            return Task.CompletedTask;
        }
    }
}