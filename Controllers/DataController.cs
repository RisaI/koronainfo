
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace koronainfo.Controllers
{
    [ApiController]
    [Route("Api/[controller]")]
    public class DataController : ControllerBase
    {
        const string CSVHeader = "date;tested;cases;deaths;recoveries\n";
        static readonly byte[] CSVHeaderBytes;

        static DataController()
        {
            CSVHeaderBytes = new byte[Encoding.UTF8.GetByteCount(CSVHeader)];
            Encoding.UTF8.GetBytes(CSVHeader, 0, CSVHeader.Length, CSVHeaderBytes, 0);
        }

        private Services.DataContainer Data;

        public DataController(Services.DataContainer data)
        {
            Data = data;
        }

        [HttpGet]
        public async Task Get([FromQuery] bool reverse = false, [FromQuery] int? skip = null, [FromQuery] bool header = true)
        {
            Response.ContentType = "text";

            var buffer = new byte[4 * 64];

            if (header)
                await Response.BodyWriter.WriteAsync(CSVHeaderBytes.AsMemory());

            IEnumerable<Data.DailyBalance> enumerable = Data.GetPrimaryCountryData().Daily;

            if (skip.HasValue)
                enumerable = enumerable.Skip(skip.Value);
            if (reverse)
                enumerable = enumerable.Reverse();

            foreach (var day in enumerable)
            {
                var line = $"{day.Date.ToString("dd-MM-yyyy")};{day.Tested};{day.Cases};{day.Deaths};{day.Recoveries}\n";
                var length = Encoding.UTF8.GetBytes(line, 0, line.Length, buffer, 0);

                await Response.BodyWriter.WriteAsync(buffer.AsMemory(0, length));
            }
            
            await Response.CompleteAsync();
        }
    }
}